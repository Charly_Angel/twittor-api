# twittor api

api de una red social como twitter utilizando nestjs + mongo

## requirements

- docker
- docker compose
- makefile

## Installation

```bash
# copy file
cp docker-compose.yml.dist docker-compose.yml

# build docker containers
$ make build
```

## Running the app

```bash
# run app
make run
```

## commands

```bash
# show commands
make help
```
