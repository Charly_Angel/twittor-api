import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '@authModule/auth.module';
import { ProfileModule } from '@profileModule/profile.module';
import { environments } from './environments';
import { FileModule } from '@fileModule/file.module';
import { TweetModule } from '@tweetModule/tweet.module';
import { FollowerModule } from '@followerModule/follower.module';
import config from './config';
import { AppController } from './app.controller';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: environments[process.env.NODE_ENV] || '.env',
      load: [config],
      isGlobal: true,
    }),
    MongooseModule.forRoot(process.env.DATABASE_URI),
    AuthModule,
    ProfileModule,
    FileModule,
    TweetModule,
    FollowerModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
