import { IsMongoId } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateFollowerDto {
  @IsMongoId()
  @ApiProperty()
  readonly userId: string;
}
