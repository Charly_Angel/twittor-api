import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, FilterQuery } from 'mongoose';
import { Follower } from '@followerModule/entities/follower.entity';
import { FollowerDto } from '@followerModule/dtos/follower.dto';
import { FilterFollowingDto } from '@followerModule/dtos/filter-following.dto';
import { FilterTweetsFromFollowingsDto } from '@tweetModule/dtos/filter-tweets-followings.dto';
import { TweetDto } from '@tweetModule/dtos/tweet.dto';
import { UserService } from '@authModule/services/user.service';

@Injectable()
export class FollowerService {
  constructor(
    @InjectModel(Follower.name) private followerModel: Model<Follower>,
    private userService: UserService,
  ) {}

  async following(followerId: string, followingId: string): Promise<void> {
    const follower = new this.followerModel({
      follower: followerId,
      following: followingId,
    });
    await follower.save();
  }

  async unFollow(followerId: string, followingId: string): Promise<void> {
    await this.followerModel
      .findOneAndDelete({ follower: followerId, following: followingId })
      .exec();
  }

  async getFollowingsByUserId(
    userId: string,
    { page }: FilterFollowingDto,
  ): Promise<FollowerDto[]> {
    const filters: FilterQuery<Follower> = { follower: userId };
    const limit = 5;

    const followings = await this.followerModel
      .find(filters, '_id message published')
      .populate({ path: 'following', select: '_id name avatar biography' })
      .skip((page - 1) * limit)
      .limit(limit)
      .sort({ published: -1 })
      .exec();

    return followings.map((follow) => ({
      id: follow.following._id.toString(),
      name: follow.following['name'],
      avatar: follow.following['avatar'],
      biography: follow.following['biography'],
    }));
  }

  async getFollowersByUserId(
    userId: string,
    { page }: FilterFollowingDto,
  ): Promise<FollowerDto[]> {
    const filters: FilterQuery<Follower> = { following: userId };
    const limit = 5;

    const followers = await this.followerModel
      .find(filters, '_id message published')
      .populate({ path: 'follower', select: '_id name avatar biography' })
      .skip((page - 1) * limit)
      .limit(limit)
      .sort({ published: -1 })
      .exec();

    return followers.map((follow) => ({
      id: follow.follower._id.toString(),
      name: follow.follower['name'],
      avatar: follow.follower['avatar'],
      biography: follow.follower['biography'],
    }));
  }

  async tweetsFromFollowings(
    userId: string,
    { page, order }: FilterTweetsFromFollowingsDto,
  ): Promise<TweetDto[]> {
    const limit = 5;

    const tweets = await this.followerModel.aggregate([
      {
        $match: { follower: userId },
      },
      {
        $lookup: {
          from: 'tweets',
          localField: 'following',
          foreignField: 'user',
          as: 'tweet',
        },
      },
      {
        $unwind: '$tweet',
      },
      {
        $sort: {
          published: order === 'asc' ? 1 : -1,
        },
      },
      {
        $skip: (page - 1) * limit,
      },
      {
        $limit: limit,
      },
    ]);

    const tweetsPromises = tweets.map(async (item) => {
      const tweet = item.tweet;
      const user = await this.userService.findByIdOrFail(tweet.user.toString());
      return {
        id: tweet._id.toString(),
        message: tweet.message,
        published: tweet.published,
        user: {
          id: user['_id'].toString(),
          name: user['name'],
          avatar: user['avatar'],
        },
      };
    });

    return await Promise.all(tweetsPromises);
  }

  async followingCheck(
    followerId: string,
    followingId: string,
  ): Promise<boolean> {
    const following = await this.followerModel
      .findOne({ follower: followerId, following: followingId })
      .exec();

    return following ? true : false;
  }
}
