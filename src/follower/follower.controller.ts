import {
  Controller,
  UseGuards,
  Param,
  Post,
  Delete,
  Body,
  Get,
} from '@nestjs/common';
import { JwtAuthGuard } from '@authModule/guards/jwt-auth.guard';
import { ApiTags } from '@nestjs/swagger';
import { MongoIdPipe } from '@common/mongo-id.pipe';
import { FollowerService } from '@followerModule/services/follower.service';
import { PayloadToken } from '@authModule/models/token.model';
import { CreateFollowerDto } from '@followerModule/dtos/create-follower.dto';
import { GetPayloadToken } from '@authModule/decorators/get-payload-token.decorator';

@UseGuards(JwtAuthGuard)
@ApiTags('follower')
@Controller('follower')
export class FollowerController {
  constructor(private followerService: FollowerService) {}

  @Post()
  following(
    @Body() createFollowerDto: CreateFollowerDto,
    @GetPayloadToken() payloadToken: PayloadToken,
  ) {
    const { userId } = createFollowerDto;
    return this.followerService.following(payloadToken.sub, userId);
  }

  @Delete(':userId')
  unFollow(
    @Param('userId', MongoIdPipe) userId: string,
    @GetPayloadToken() payloadToken: PayloadToken,
  ) {
    return this.followerService.unFollow(payloadToken.sub, userId);
  }

  @Get(':userId/check')
  checkFollowing(
    @Param('userId', MongoIdPipe) userId: string,
    @GetPayloadToken() payloadToken: PayloadToken,
  ) {
    return this.followerService.followingCheck(payloadToken.sub, userId);
  }
}
