import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { PayloadToken } from '@authModule/models/token.model';

export const GetPayloadToken = createParamDecorator(
  (_data, ctx: ExecutionContext): PayloadToken => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
  },
);
