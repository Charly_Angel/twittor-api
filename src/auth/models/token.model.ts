export interface PayloadToken {
  sub: string;
  name: string;
  email: string;
}
