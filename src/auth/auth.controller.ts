import {
  Body,
  Controller,
  Post,
  UseGuards,
  Req,
  HttpCode,
  Patch,
} from '@nestjs/common';
import { ApiTags, ApiBody } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { UserService } from '@authModule/services/user.service';
import { User } from '@authModule/entities/user.entity';
import { CreateUserDto } from '@authModule/dtos/user.dto';
import { LoginDto } from '@authModule/dtos/login.dto';
import { AuthService } from '@authModule/services/auth.service';
import { RequestResetPasswordDto } from '@authModule/dtos/request-reset-password.dto';
import { ResetPasswordDto } from '@authModule/dtos/reset-password.dto';
import { ChangePasswordDto } from '@authModule/dtos/change-password.dto';
import { GetPayloadToken } from '@authModule/decorators/get-payload-token.decorator';
import { PayloadToken } from '@authModule/models/token.model';
import { JwtAuthGuard } from '@authModule/guards/jwt-auth.guard';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}

  @Post('/register')
  register(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @ApiBody({ type: LoginDto })
  @UseGuards(AuthGuard('local'))
  @HttpCode(200)
  @Post('/login')
  login(@Req() req: Request) {
    const user = req.user as User;
    return this.authService.generateJWT(user);
  }

  @Patch('/request-reset-password')
  requestResetPassword(
    @Body() requestResetPassword: RequestResetPasswordDto,
  ): Promise<void> {
    return this.authService.requestResetPassword(requestResetPassword);
  }

  @Patch('/reset-password')
  resetPassword(@Body() resetPasswordDto: ResetPasswordDto): Promise<void> {
    return this.authService.resetPassword(resetPasswordDto);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/change-password')
  changePassword(
    @Body() changePasswordDto: ChangePasswordDto,
    @GetPayloadToken() payloadToken: PayloadToken,
  ): Promise<void> {
    return this.authService.changePassword(payloadToken.sub, changePasswordDto);
  }
}
