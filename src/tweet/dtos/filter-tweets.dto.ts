import { IsNumber, Min } from 'class-validator';

export class FilterTweetsDto {
  @IsNumber()
  @Min(1)
  page: number;
}
