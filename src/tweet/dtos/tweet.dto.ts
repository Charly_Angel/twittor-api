import { ApiProperty } from '@nestjs/swagger';

class UserTweetDto {
  @ApiProperty()
  readonly id: string;

  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly avatar: string;
}

export class TweetDto {
  @ApiProperty()
  readonly id: string;

  @ApiProperty()
  readonly message: string;

  @ApiProperty()
  readonly published: Date;

  @ApiProperty()
  readonly user: UserTweetDto;
}
