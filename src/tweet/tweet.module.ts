import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TweetController } from '@tweetModule/tweet.controller';
import { TweetService } from '@tweetModule/services/tweet.service';
import { AuthModule } from '@authModule/auth.module';
import { Tweet, TweetSchema } from '@tweetModule/entities/tweet.entity';

@Module({
  imports: [
    AuthModule,
    MongooseModule.forFeature([
      {
        name: Tweet.name,
        schema: TweetSchema,
      },
    ]),
  ],
  controllers: [TweetController],
  providers: [TweetService],
  exports: [TweetService],
})
export class TweetModule {}
