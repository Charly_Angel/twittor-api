import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, FilterQuery } from 'mongoose';
import { Tweet } from '@tweetModule/entities/tweet.entity';
import { CreateTweetDto } from '@tweetModule/dtos/create-tweet.dto';
import { FilterTweetsDto } from '@tweetModule/dtos/filter-tweets.dto';
import { TweetDto } from '@tweetModule/dtos/tweet.dto';
@Injectable()
export class TweetService {
  constructor(@InjectModel(Tweet.name) private tweetModel: Model<Tweet>) {}

  async saveTweet(id: string, createTweetDto: CreateTweetDto): Promise<void> {
    const tweet = new this.tweetModel({ user: id, ...createTweetDto });
    await tweet.save();
  }

  async deleteTweet(userId: string, tweetId: string): Promise<void> {
    await this.tweetModel
      .findOneAndUpdate({ _id: tweetId, userId }, { $set: { active: false } })
      .exec();
  }

  async tweetsByUserId(
    userId: string,
    { page }: FilterTweetsDto,
  ): Promise<TweetDto[]> {
    const filters: FilterQuery<Tweet> = { user: userId, active: true };
    const limit = 5;

    const tweets = await this.tweetModel
      .find(filters, '_id message published')
      .populate({ path: 'user', select: '_id name avatar' })
      .skip((page - 1) * limit)
      .limit(limit)
      .sort({ published: -1 })
      .exec();

    return tweets.map((tweet) => ({
      id: tweet._id.toString(),
      message: tweet.message,
      published: tweet.published,
      user: {
        id: tweet.user._id.toString(),
        name: tweet.user['name'],
        avatar: tweet.user['avatar'],
      },
    }));
  }
}
