import {
  Controller,
  UseGuards,
  Post,
  Body,
  Delete,
  Param,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '@authModule/guards/jwt-auth.guard';
import { TweetService } from '@tweetModule/services/tweet.service';
import { CreateTweetDto } from '@tweetModule/dtos/create-tweet.dto';
import { PayloadToken } from '@authModule/models/token.model';
import { MongoIdPipe } from '@common/mongo-id.pipe';
import { GetPayloadToken } from '@authModule/decorators/get-payload-token.decorator';

@UseGuards(JwtAuthGuard)
@ApiTags('tweet')
@Controller('tweet')
export class TweetController {
  constructor(private tweetService: TweetService) {}

  @Post()
  tweet(
    @Body() createTweetDto: CreateTweetDto,
    @GetPayloadToken() payloadToken: PayloadToken,
  ) {
    this.tweetService.saveTweet(payloadToken.sub, createTweetDto);
  }

  @Delete('/:id')
  deleteTweet(
    @Param('id', MongoIdPipe) id: string,
    @GetPayloadToken() payloadToken: PayloadToken,
  ) {
    this.tweetService.deleteTweet(payloadToken.sub, id);
  }
}
