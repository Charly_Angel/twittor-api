import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsEmail, IsOptional, IsString, IsUrl } from 'class-validator';

export class UpdateProfileDto {
  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly name: string;

  @IsString()
  @IsOptional()
  @IsEmail()
  @ApiProperty()
  readonly email: string;

  @IsOptional()
  @IsDate()
  @ApiProperty()
  readonly birthday: Date;

  @IsOptional()
  @IsUrl()
  @IsString()
  @ApiProperty()
  readonly website: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly location: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly biography: string;
}
