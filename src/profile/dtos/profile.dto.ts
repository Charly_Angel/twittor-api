import { ApiProperty } from '@nestjs/swagger';
import {
  IsDate,
  IsEmail,
  IsMongoId,
  IsOptional,
  IsString,
  IsUrl,
} from 'class-validator';

export class ProfileDto {
  @IsMongoId()
  @ApiProperty()
  readonly id: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly name: string;

  @IsString()
  @IsOptional()
  @IsEmail()
  @ApiProperty()
  readonly email: string;

  @IsOptional()
  @IsDate()
  @ApiProperty()
  readonly birthday: Date;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly avatar: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly banner: string;

  @IsOptional()
  @IsUrl()
  @IsString()
  @ApiProperty()
  readonly website: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly location: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly biography: string;
}
