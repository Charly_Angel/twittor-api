import { ApiProperty } from '@nestjs/swagger';

export class BannerUploadDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  banner: any;
}
