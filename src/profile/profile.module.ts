import { Module } from '@nestjs/common';
import { ProfileService } from '@profileModule/services/profile.service';
import { ProfileController } from '@profileModule/profile.controller';
import { AuthModule } from '@authModule/auth.module';
import { FileModule } from '@fileModule/file.module';
import { TweetModule } from '@tweetModule/tweet.module';
import { FollowerModule } from '@followerModule/follower.module';

@Module({
  imports: [AuthModule, FileModule, TweetModule, FollowerModule],
  controllers: [ProfileController],
  providers: [ProfileService],
})
export class ProfileModule {}
