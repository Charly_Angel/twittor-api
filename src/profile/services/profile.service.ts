import { Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { UserService } from '@authModule/services/user.service';
import { ProfileDto } from '@profileModule/dtos/profile.dto';
import { UpdateProfileDto } from '@profileModule/dtos/update-profile.dto';
import config from '@src/config';
import { FileService } from '@fileModule/file.service';

@Injectable()
export class ProfileService {
  constructor(
    @Inject(config.KEY) private configService: ConfigType<typeof config>,
    private userService: UserService,
    private fileService: FileService,
  ) {}

  public async searchProfiles(query: string): Promise<ProfileDto[]> {
    const users = await this.userService.findByNameQuery(query);

    return users.map((user) => ({
      id: user._id.toString(),
      avatar: user['avatar'],
      banner: user['banner'],
      birthday: user['birthday'],
      website: user['website'],
      name: user.name,
      email: user.email,
      location: user['location'],
      biography: user['biography'],
    }));
  }

  public async getProfileById(id: string): Promise<ProfileDto> {
    const user = await this.userService.findByIdOrFail(id);
    delete user.password;
    const profile: ProfileDto = {
      id: user._id.toString(),
      avatar: user['avatar'],
      banner: user['banner'],
      birthday: user['birthday'],
      website: user['website'],
      name: user.name,
      email: user.email,
      location: user['location'],
      biography: user['biography'],
    };
    return profile;
  }

  public async updateProfile(
    id: string,
    changes: UpdateProfileDto,
  ): Promise<void> {
    await this.userService.update(id, changes);
  }

  public async updateAvatar(
    id: string,
    file: Express.Multer.File,
  ): Promise<void> {
    const user = await this.getProfileById(id);
    if (user?.avatar) {
      await this.fileService.deleteFileCloudinary(
        user.avatar,
        'avatar',
        'image',
      );
    }

    const url = await this.fileService.uploadFileCloudinary('avatar', file);
    await this.userService.updateAvatar(id, url);
  }

  public async updateBanner(
    id: string,
    file: Express.Multer.File,
  ): Promise<void> {
    const user = await this.getProfileById(id);
    if (user?.banner) {
      await this.fileService.deleteFileCloudinary(
        user.banner,
        'banner',
        'image',
      );
    }

    const url = await this.fileService.uploadFileCloudinary('banner', file);
    await this.userService.updateBanner(id, url);
  }
}
