import { Injectable, BadRequestException, Inject } from '@nestjs/common';
import { join } from 'path';
import { unlinkSync } from 'fs';
import { ConfigType } from '@nestjs/config';
import config from '@src/config';
import { v2 as cloudinary } from 'cloudinary';
import { Logger } from '@nestjs/common';

@Injectable()
export class FileService {
  constructor(
    @Inject(config.KEY) private configService: ConfigType<typeof config>,
  ) {}

  deleteFile(folder: string, file: string): void {
    const filePath = join(__dirname, `../../uploads/${folder}/${file}`);
    try {
      unlinkSync(filePath);
    } catch (err) {
      console.error(`file ${filePath} no found`);
    }
  }

  async uploadFileCloudinary(
    folder: string,
    file: Express.Multer.File,
  ): Promise<string> {
    cloudinary.config({
      secure: true,
      api_key: this.configService.cloudinary.apiKey,
      api_secret: this.configService.cloudinary.apiSecret,
      cloud_name: this.configService.cloudinary.cloudName,
    });

    const options = {
      use_filename: true,
      unique_filename: false,
      overwrite: true,
      folder: `twittor/${folder}`,
    };
    try {
      // Upload the image
      const result = await cloudinary.uploader.upload(file.path, options);
      this.deleteFile(folder, file.filename);
      return result.secure_url;
    } catch (error) {
      throw new BadRequestException('Error al subir la imagen');
    }
  }

  async deleteFileCloudinary(
    url: string,
    folder: string,
    resource_type: string,
  ): Promise<void> {
    cloudinary.config({
      secure: true,
      api_key: this.configService.cloudinary.apiKey,
      api_secret: this.configService.cloudinary.apiSecret,
      cloud_name: this.configService.cloudinary.cloudName,
    });

    const segments = url.split('/');
    const urlId = segments[segments.length - 1].split('.')[0];
    console.log('urlId: ', urlId);

    try {
      await cloudinary.api.delete_resources([`twittor/${folder}/${urlId}`], {
        resource_type,
      });
    } catch (error) {
      const logger = new Logger('cloudinary');
      logger.log('Error al eliminar imagen de cloudinary');
    }
  }
}
