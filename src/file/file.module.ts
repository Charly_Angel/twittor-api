import { Module } from '@nestjs/common';
import { FileService } from '@fileModule/file.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  providers: [FileService],
  exports: [FileService],
  imports: [ConfigModule],
})
export class FileModule {}
